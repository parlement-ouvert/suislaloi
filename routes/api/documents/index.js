import documents from '../_data.json';

const contents = JSON.stringify(documents.map(document => {
	return {
		slug: document.slug,
		titre: document.titre,
		type: document.type,
	};
}));

export function get(req, res) {
	res.set({
		'Content-Type': 'application/json'
	});

	res.end(contents);
}