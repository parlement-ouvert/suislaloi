import documents from "../_data.json"

const lookup = new Map()
documents.forEach(document => {
	lookup.set(document.slug, JSON.stringify(document))
})

export function get(req, res, next) {
	// the `slug` parameter is available because this file
	// is called [slug].js
	const { slug } = req.params

	if (lookup.has(slug)) {
		res.set({
			"Content-Type": "application/json",
		})

		res.end(lookup.get(slug))
	} else {
		next()
	}
}
